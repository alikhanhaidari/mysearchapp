@extends('layouts.app')

@section('content')
 <div class="container table-responsive">  
    <table class="table table-bordered table-hover thread-dark mt-5 " style="background-color:white;">
        <tr class="d-flex" style="background-color: black; color: white;">
        <th class="col-md-1">#</th>
        <th class="col-md-9">File Name</th>
        <!-- <th class="col-md-1">Size (KB)</th>
        <th class="col-md-2">Type</th> -->
        <th class="col-md-2" >Action</th>
        </tr>
        <?php $i=1;?>
        @foreach ($contents as $con)  
        <?php $link = urlencode($con['entity']); ?>
        <tr class="d-flex">
        <td class="col-md-1"><b>{{$i}}</b></td>
        <td class="col-md-9"><b>{{$con['basename']}}</b></td>
        <?php $i++; ?>
        <!-- <td class="col-md-1"<a href="{{$con['entity']}}" target="_blank">{{$con['basename']}}</a></td> -->
        <!-- <td class="col-md-1">{{$con['size']}} kb </td>
        <td class="col-md-2">{{$con['type']}}</td>  -->
        <td class="col-md-2">
        <div class="dropdown show">
        <a class="btn btn-dark dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
           Select Action
        </a> 
        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
        <a href="/openfile/{{$link}}" target="_blank" id="openfile" class="dropdown-item">open</a>
        <a href="/openfolder/{{$link}}" target="_blank" id="openfile" class="dropdown-item">open folder</a> 
        <a href="/metadata/extract/{{$link}}" target="_blank" id="openfile" class="dropdown-item">Generate General Metadata</a>
        <!-- <a href="#" target="_blank" id="openfile" class="dropdown-item">Edit Metadata</a> 
        <a href="#" target="_blank" id="openfile" class="dropdown-item">Delete Metadata</a>
        <a href="#" target="_blank" id="openfile" class="dropdown-item">Generate Relational Metadata</a>  -->
        </div>
        </div>
        </td>
        </tr>  
        @endforeach
    </table>  
</div>

@endsection
 

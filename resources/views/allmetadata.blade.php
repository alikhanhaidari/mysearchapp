@extends('layouts.app')

@section('content')
<script>
$(document).ready(function () {
$('#dtBasicExample').DataTable(); 
$('#dtBasicExample1').DataTable();
});
</script>
<div class="container-fluid">

<div class="row">
<div class="col-2"></div>


<div class="col-8">
<div class="row">
  <div class="col-1"></div>
  <div class="col-10 rounded bg-light py-3" style="margin-top:30px"> 
    <h5 class="text-center">General Meta Data <strong class="text-danger">{{$cobj[0]->filename}}</strong></h5>
    <table class="table w-auto small table-bordered " cellspacing="0" width="100%">
    <thead>
      <tr style="">
        <th class="th-sm">#</th>
        <th class="th-sm">Metadata</th>
        <th class="th-sm">Value</th> 
      </tr>
    </thead>
    <tbody> 
      <tr><td>1</td><td>ID</td><td>{{$cobj[0]->id}}</td></tr>
      <tr><td>2</td><td>Author Name</td><td>{{$cobj[0]->author}}</td></tr>
      <tr><td>3</td><td>Title</td><td>{{$cobj[0]->title}}</td></tr>
      <tr><td>4</td><td>File Name</td><td>{{$cobj[0]->filename}}</td></tr>
      <tr><td>5</td><td>Extension</td><td>{{$cobj[0]->extension}}</td></tr>
      <tr><td>6</td><td>Number of Pages</td><td>{{$cobj[0]->pages}}</td></tr> 
      <tr><td>7</td><td>Modified Date</td><td>{{$cobj[0]->moddate}}</td></tr>
      <tr><td>8</td><td>Creation Date</td><td>{{$cobj[0]->creationdate}}</td></tr>
      <tr><td>9</td><td>Mime Type</td><td>{{$cobj[0]->mime}}</td></tr>
      <tr><td>10</td><td>Size (KB)</td><td>{{$cobj[0]->size}}</td></tr>
      <tr><td>11</td><td>URL/Path</td><td>{{$cobj[0]->url}}</td></tr>
      <tr><td>12</td><td>Purpose</td><td>{{$cobj[0]->purpose}}</td></tr>
      <tr><td>12</td><td>Keywords</td><td>{{$cobj[0]->keywords}}</td></tr>
      <tr><td>13</td><td>Comments</td><td>{{$cobj[0]->comment}}</td></tr>
    </tbody>
  </table>

  </div> 
</div>
</div>



</div>
</div>
@endsection
 

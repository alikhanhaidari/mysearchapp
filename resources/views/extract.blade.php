@extends('layouts.app')

@section('content')
    <div class="container">
                <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6 bg-light rounded py-3 text-center" style="margin-top:80px"> 
                <h1 class="text-center">Generated Metadata</h1> 
                {!! Form::open(array('url' => 'metadata/extracted','class'=>'form-inline','id'=>'loginform','role'=>'form', 'method'=>'post', 'enctype'=>'multipart/form-data')) !!}

                
                <div class="form-group my-3 col-md-12">
                @if (Session::has('message'))
                <div class="alert alert-success w-100">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true" >&times;</span></button>
                {{ Session::get('message') }}</div>
                @endif

                @if (Session::has('error'))
                <div class="alert alert-danger w-100">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true" >&times;</span></button>
                {{ Session::get('error') }}</div>
                @endif

                </div>

                <div class="form-group my-3 col-md-6">
                <p style="padding-left: 12px;">Title:</p>
                <input type="text" class="form-control col-12" value="{{$data['Title']}}" name="title" placeholder="Title">
                </div>
                <div class="form-group my-3 col-md-6">
                <p style="padding-left: 12px;">Author:</p>
                <input type="text" class="form-control col-12" value="{{$data['Author']}}" name="author" placeholder="Author">
                </div> 
                <div class="form-group my-3 col-md-6">
                <p style="padding-left: 12px;">Modification Date:</p>
                <input type="text" class="form-control col-12" value="{{$data['ModDate']}}" name="moddate" placeholder="Modification Date">
                </div>
                <div class="form-group my-3 col-md-6">
                <p style="padding-left: 12px;">Creation Date:</p>
                <input type="text" class="form-control col-12" value="{{$data['CreationDate']}}" name="creationdate" placeholder="Creation Date">
                </div>
                <div class="form-group my-3 col-md-6">
                <p style="padding-left: 12px;">No. of Pages:</p>
                <input type="text" class="form-control col-12" value="{{$data['Pages']}}" name="pages" placeholder="No. of Pages">
                </div>
                <div class="form-group my-3 col-md-6 pt-5"> 
                <input type="hidden" class="form-control col-12" value="{{$data['url']}}" name="url">
                <button type="submit" class="btn bg-main-color mb-2">Submit Metadata</button>
                </div> 
                <!-- <div class="form-group my-3 col-md-6"> 
                </div>
                <div class="form-group my-3 col-md-6">
                <button type="submit" class="btn btn-warning mb-2">Submit Metadata</button>
                </div>   -->
                {!! Form::close() !!} 
                </div>  
                </div> 
    </div>
@endsection

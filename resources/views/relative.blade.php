@extends('layouts.app')

@section('content')
<script>
$(document).ready(function () {
$('#dtBasicExample').DataTable(); 
$('#dtBasicExample1').DataTable();
});
</script>
<div class="container-fluid">
<!-- <div class="row">
<div class="col-12 text-center py-4">
<h1 class="text-primary">Add Relative Meta Data To</h1> <br>
<h3 class="text-Warning">{{$cobj[0]->title}}</h3>
</div>
</div> -->

<div class="row">
<div class="col-3">
<div class="container bg-light my-3 rounded">
<h5 class="py-3">Learning Objects Metadata</h5>
<a href="0" class="btn bg-main-color my-3 w-100">General</a>
<a href="1" class="btn bg-main-color my-3 w-100">Relational</a>
<a href="2" class="btn bg-main-color my-3 w-100">Classification</a>
<a href="3" class="btn bg-main-color my-3 w-100">Technical</a>
<!-- <a href="4" class="btn bg-main-color my-3 w-100">Rights</a> -->
<a href="5" class="btn bg-main-color my-3 w-100">Annotation</a>
</div>
</div>





@if($option == 0) 
<div class="col-9">
<div class="row">
  <div class="col-1"></div>
  <div class="col-10 rounded bg-light py-3" style="margin-top:30px"> 
    <h5 class="text-center">General Meta Data <strong class="text-danger">{{$cobj[0]->filename}}</strong></h5>
    <table class="table w-auto small table-bordered " cellspacing="0" width="100%">
    <thead>
      <tr style="">
        <th class="th-sm">#</th>
        <th class="th-sm">Metadata</th>
        <th class="th-sm">Value</th> 
      </tr>
    </thead>
    <tbody> 
      <tr><td>1</td><td>ID</td><td>{{$cobj[0]->id}}</td></tr>
      <tr><td>2</td><td>Author Name</td><td>{{$cobj[0]->author}}</td></tr>
      <tr><td>3</td><td>Title</td><td>{{$cobj[0]->title}}</td></tr>
      <tr><td>4</td><td>File Name</td><td>{{$cobj[0]->filename}}</td></tr>
      <tr><td>5</td><td>Extension</td><td>{{$cobj[0]->extension}}</td></tr>
      <tr><td>6</td><td>Number of Pages</td><td>{{$cobj[0]->pages}}</td></tr> 
      <tr><td>7</td><td>Modified Date</td><td>{{$cobj[0]->moddate}}</td></tr>
      <tr><td>8</td><td>Creation Date</td><td>{{$cobj[0]->creationdate}}</td></tr>
    </tbody>
  </table>

  </div> 
</div>
</div>

@endif





@if($option == 1)
<div class="col-9">

<div class="bg-light py-3 my-3 rounded"> 
<div class="col-12 text-center py-2">
<h5>Add Relative Meta Data To <strong class="text-danger">{{$cobj[0]->filename}}</strong></h5>
</div>
<h5 class="text-success text-center py-1"> All Available Objects </h5>
<div class="container table-responsive ">
@if(isset($data))
<table id="dtBasicExample" class="table w-auto small table-striped table-bordered " cellspacing="0" width="100%">
  <thead>
    <tr style="background-color: black; color: white;">
      <th class="th-sm">#</th>
      <th class="th-sm">File Name</th>
      <th class="th-sm">Author</th>
      <th class="th-sm">Actions</th>
    </tr>
  </thead>
  <tbody>
  <?php $i = 1; $clink = urlencode($cobj[0]->url); ?>
  @foreach ($data as $d)
  <?php $link = urlencode($d->url); ?>
    <tr>
        <td>{{$i}}</td>
        <td><a href="/openfile/{{$link}}" target="_blank" class="dropdown-item">{{$d->filename}}</a></td>
        <td>{{$d->author}}</td>
        <td>
        <a class="btn btn-success btn-sm" href="/addRelation/{{$clink}}/{{$cobj[0]->id}}/{{$d->id}}">Add</a> 
        </td>
    </tr>
<?php $i = $i + 1; ?>
  @endforeach
    </tbody>
</table>
@endif
</div>
<!-- </div>
<div class="col-6"> -->
<h3 class="text-danger text-center py-2 mt-2"> All Selected Objects </h3>
<div class="container table-responsive">
@if(isset($relobj))
<table id="dtBasicExample1" class="table w-auto small table-striped table-bordered " cellspacing="0" width="100%">
  <thead>
    <tr style="background-color: black; color: white;">
      <th class="th-sm">#</th>
      <th class="th-sm">File Name</th>
      <th class="th-sm">Author</th>
      <th class="th-sm">Actions</th>
    </tr>
  </thead>
  <tbody>
  <?php $ind = 1; ?>
  @foreach ($relobj as $rd)
  <?php $link = urlencode($rd->url);?>
    <tr>
        <td>{{$ind}}</td>
        <td><a href="/openfile/{{$link}}" target="_blank" class="dropdown-item">{{$rd->filename}}</a></td>
        <td>{{$rd->author}}</td>
        <td>
        <a class="btn btn-danger btn-sm" href="/removeRelation/{{$clink}}/{{$cobj[0]->id}}/{{$rd->id}}">Remove</a> 
        </td>
    </tr>
<?php $ind = $ind + 1; ?>
  @endforeach
    </tbody>
</table>
@else
<p>No relational objects yet!</p>
@endif
</div>
</div>
@endif



@if($option == 2)
<div class="col-9">
<div class="row">
  <div class="col-3"></div>
  <div class="col-6 rounded bg-light" style="margin-top:30px">
  <div class="form-group my-3 col-12">
  @if (Session::has('message'))
  <div class="alert alert-success w-100">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true" >&times;</span></button>
  {{ Session::get('message') }}</div>
  @endif
  </div>
  {!! Form::open(array('url' => 'metadata/edit/2','class'=>'form-inline, w-100','id'=>'loginform','role'=>'form', 'method'=>'post')) !!}
    <h2 class="text-center my-2 mb-3">Classification Metadata</h2>
    <div class="form-group my-3">
    <p style="padding-left: 12px;">Purpose:</p>
    <textarea class="form-control col-12"  name="purpose" placeholder="Purpose" rows="5">{{$cobj[0]->purpose}}</textarea>
    </div>
    <div class="form-group my-3">
    <p style="padding-left: 12px;">Keywords:</p>
    <textarea class="form-control col-12" name="keywords" placeholder="keyword1;keyword2;keyword3..." rows="5">{{$cobj[0]->keywords}}</textarea>
    </div> 
    <div class="form-group my-3">
    <input type="hidden" class="form-control col-12" value="{{$cobj[0]->url}}" name="url">
    </div>
    <div class="form-group my-3 float-right">
    <button type="submit" class="btn bg-main-color">Submit</button>
    </div>
  </div>
  {!! Form::close() !!}
</div>
</div>
@endif

@if($option == 3)
<div class="col-9">
<div class="row">
  <div class="col-3"></div>
  <div class="col-6 rounded bg-light" style="margin-top:80px">
  {!! Form::open(array('url' => 'metadata/edit/3','class'=>'form-inline, w-100','id'=>'loginform','role'=>'form', 'method'=>'post')) !!}
    <h2 class="text-center my-2 mb-3">Technical Metadata</h2>
    <div class="form-group my-3">
    <p style="padding-left: 12px;">Format:</p>
    <input type="text" class="form-control col-12" value="{{$cobj[0]->mime}}" name="format" placeholder="Format">
    </div>
    <div class="form-group my-3">
    <p style="padding-left: 12px;">Size:</p>
    <input type="text" class="form-control col-12" value="{{$cobj[0]->size}}" name="size" placeholder="Size">
    </div> 
    <div class="form-group my-3">
    <p style="padding-left: 12px;">Location:</p>
    <input type="text" class="form-control col-12" value="{{$cobj[0]->url}}" name="location" placeholder="Location">
    <input type="hidden" class="form-control col-12" value="{{$cobj[0]->url}}" name="url">
    </div>
    <div class="form-group my-3 float-right">
    <button type="submit" class="btn bg-main-color" disabled>Submit</button>
    </div>
  </div>
  {!! Form::close() !!}
</div>
</div>
@endif

@if($option == 4)
<!-- <div class="col-9">
<div class="row">
  <div class="col-3"></div>
  <div class="col-6 rounded bg-light" style="margin-top:30px">
  {!! Form::open(array('url' => 'metadata/edit/4','class'=>'form-inline, w-100','id'=>'loginform','role'=>'form', 'method'=>'post')) !!}
    <h2 class="text-center my-2 mb-3">Rights Metadata</h2>
    <div class="form-group my-3">
    <p style="padding-left: 12px;">Description:</p>
    <textarea class="form-control col-12" name="description" placeholder="Description" rows="5">{{$cobj[0]->description}}</textarea>
    </div>
    <div class="form-group my-3">
    <p style="padding-left: 12px;">Copy Rights & Restrictions:</p>
    <textarea class="form-control col-12" name="copyrights" placeholder="Copy Rights and Restrictions" rows="5">{{$cobj[0]->copyrights}}</textarea>
    </div> 
    <div class="form-group my-3"> 
    <input type="hidden" class="form-control col-12" value="{{$cobj[0]->url}}" name="url">
    </div>
    <div class="form-group my-3 float-right">
    <button type="submit" class="btn bg-main-color">Submit</button>
    </div>
  </div>
  {!! Form::close() !!}
</div>
</div> -->
@endif

@if($option == 5)
<div class="col-9">
<div class="row">
  <div class="col-3"></div>
  <div class="col-6 rounded bg-light" style="margin-top:80px">

  <div class="form-group my-3 col-12">
  @if (Session::has('message'))
  <div class="alert alert-success w-100">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
  <span aria-hidden="true" >&times;</span></button>
  {{ Session::get('message') }}</div>
  @endif
  </div>

  {!! Form::open(array('url' => 'metadata/edit/5','class'=>'form-inline, w-100','id'=>'loginform','role'=>'form', 'method'=>'post')) !!}
    <h2 class="text-center my-2 mb-3">Annotation Metadata</h2>
    <div class="form-group my-3">
    <p style="padding-left: 12px;">Comment:</p>
    <textarea class="form-control col-12" value="" name="comment" placeholder="Comment" rows="10">{{$cobj[0]->comment}}</textarea>
    </div>
    <div class="form-group my-3">
    <input type="hidden" class="form-control col-12" value="{{$cobj[0]->url}}" name="url">
    </div>
    <div class="form-group my-3 float-right">
    <button type="submit" class="btn bg-main-color">Submit</button>
    </div>
  </div>
  {!! Form::close() !!}
</div>
</div>
@endif

</div>
</div>
@endsection
 

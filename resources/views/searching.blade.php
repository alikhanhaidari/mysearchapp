@extends('layouts.app')

@section('content')
<script>
$(document).ready(function () {
$('#dtBasicExample').DataTable();
$('.dataTables_length').addClass('bs-select');

//title suggestions
$('#titleinput').keyup(function(){
  var q = $('#titleinput').val();
  if (q.length > 0) {
    $.ajax({url: "{{ url('/filtersearch') }}",method: 'post',data: {value: q,column: 'title',},
        success: function(result){
          $('.titlelistgroup').css("display", "block");  
          var $data = '';
            for (let index = 0; index < result.data.length; index++) {const element = result.data[index].title;
              $data = $data + '<li class="list-group-item" style="z-index:1; width:100%; cursor:pointer;"><a id="titlelist">'+ element +'</a></li>\n';}
            $('.titlelistgroup').html($data);}});
  }else{$('.titlelistgroup').css("display", "none");}});
$(document).on('click', '#titlelist', function(){$('#titleinput').val($(this).text());$('.titlelistgroup').css("display", "none");});


//filename suggestions
$('#filenameinput').keyup(function(){
  var q = $('#filenameinput').val();
  if (q.length > 0) {
    $.ajax({url: "{{ url('/filtersearch') }}",method: 'post',data: {value: q,column: 'filename',},
        success: function(result){
          $('.filenamelistgroup').css("display", "block");  
          var $data = '';
            for (let index = 0; index < result.data.length; index++) {const element = result.data[index].filename;
              $data = $data + '<li class="list-group-item" style="z-index:1; width:100%; cursor:pointer;"><a id="filenamelist">'+ element +'</a></li>\n';}
            $('.filenamelistgroup').html($data);}});
  }else{$('.filenamelistgroup').css("display", "none");}});
$(document).on('click', '#filenamelist', function(){$('#filenameinput').val($(this).text());$('.filenamelistgroup').css("display", "none");});



//author suggestions
$('#authorinput').keyup(function(){
  var q = $('#authorinput').val();
  if (q.length > 0) {
    $.ajax({url: "{{ url('/filtersearch') }}",method: 'post',data: {value: q,column: 'author',},
        success: function(result){
          $('.authorlistgroup').css("display", "block");  
          var $data = '';
            for (let index = 0; index < result.data.length; index++) {const element = result.data[index].author;
              $data = $data + '<li class="list-group-item" style="z-index:1; width:100%; cursor:pointer;"><a id="authorlist">'+ element +'</a></li>\n';}
            $('.authorlistgroup').html($data);}});
  }else{$('.authorlistgroup').css("display", "none");}});
$(document).on('click', '#authorlist', function(){$('#authorinput').val($(this).text());$('.authorlistgroup').css("display", "none");});

});

</script>
<div class="container-fluid">
<div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6 bg-light rounded py-5" style="margin-top:100px">  
    <!-- <form class="form-inline">  -->
    {!! Form::open(array('url' => '/','class'=>'form-inline','id'=>'loginform','role'=>'form', 'autocomplete'=>'off', 'method'=>'post', 'enctype'=>'multipart/form-data')) !!}
    <div class="row">
    <div class="form-group mb-2 col-6 px-1">
    <p style="padding-left: 12px; margin-bottom: 0.2rem;">File Name:</p>
    <input type="text" class="form-control col-12" name="filename" placeholder="File Name" id="filenameinput">
    <div style="height:1px; width:100%">
    <ul class="list-group filenamelistgroup w-100">
    </ul>
    </div>
    </div>
    <div class="form-group mb-2 col-6 px-1">
    <p style="padding-left: 12px; margin-bottom: 0.2rem;">Title:</p>
    <input type="text" class="form-control col-12" name="title" placeholder="Title" id="titleinput">
    <div style="height:2px; width:100%">
    <ul class="list-group titlelistgroup w-100">
    </ul>
    </div>
    </div>
    <div class="form-group mb-2 col-6 px-1">
    <p style="padding-left: 12px; margin-bottom: 0.2rem;">Author:</p>
    <input type="text" class="form-control col-12" name="author" placeholder="Author" id="authorinput">
    <div style="height:2px; width:100%">
    <ul class="list-group authorlistgroup w-100">
    </ul>
    </div>
    </div>
    <div class="form-group mb-2 col-6 px-1" >
    <p style="padding-left: 12px; margin-bottom: 0.2rem;">Extension:</p>
    <input type="text" class="form-control col-12" name="extension" style="z-index:0;" placeholder="Extension" id="extensioninput">
    </div>

    <div class="form-group mb-2 col-6 px-1">
    <p style="padding-left: 12px; margin-bottom: 0.2rem;">Keywords:</p>
    <input type="text" class="form-control col-12" name="keywords" placeholder="Keywords" id="keywordsinput">
    </div>
    <div class="form-group mb-2 col-6 px-1" >
    <p style="padding-left: 12px; margin-bottom: 0.2rem;">Size:</p>
    <input type="text" class="form-control col-12" name="size" style="z-index:0;" placeholder="Size" id="sizeinput">
    </div>

    <div class="form-group mb-2 col-6 px-1">
    <p style="padding-left: 12px; margin-bottom: 0.2rem;">No. Pages:</p>
    <input type="text" class="form-control col-12" name="pages" placeholder="No. Pages">
    </div>
    <div class="form-group mb-2 col-6 px-1">
    <button type="submit" class="btn bg-main-color" style="z-index:0; margin-top:27px"><i class="fa fa-search"></i> Search</button>
    </div>
    </div>
    <!-- </form> -->
    {!! Form::close() !!} 
    </div>  
    </div> 
</div>
<div class="container table-responsive py-2">
@if(isset($data))
@foreach ($data as $d)
<!-- <p>{{$d->id}}</p> -->
@endforeach
<table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
  <thead>
    <tr style="background-color: black; color: white;">
      <th class="th-sm">#</th>
      <th class="th-sm">File Name</th>
      <th class="th-sm">Author</th>
      <th class="th-sm">Title</th>
      <!-- <th class="th-sm">Ext.</th>
      <th class="th-sm">Size (KB)</th> -->
      <th class="th-sm">Actions</th>
    </tr>
  </thead>
  <tbody>
  <?php $i = 1; ?>
  @foreach ($data as $d)
  <?php $link = urlencode($d->url);?>
    <tr>
        <td>{{$i}}</td>
        <td><a href="/openfile/{{$link}}" target="_blank" class="dropdown-item">{{$d->filename}}</a></td>
        <td>{{$d->author}}</td>
        <td>{{$d->title}}</td>
        <!-- <td>{{$d->extension}}</td>
        <td>{{$d->size}}</td> -->
        <td>
        <div class="dropdown show">
        <a class="btn btn-dark dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
           Select Action
        </a> 
        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
        <a href="/openfile/{{$link}}" target="_blank" class="dropdown-item">open</a>
        <a href="/openfolder/{{$link}}" target="_blank" class="dropdown-item">open folder</a>
        <a href="/allmetadata/{{$link}}" target="_blank" class="dropdown-item">View Metadata</a> 
        <!-- <a href="/metadata/edit/{{$link}}" class="dropdown-item" data-toggle="modal" data-target="#exampleModalCenter">Edit Metadata</a>  -->
        <!-- <a class="dropdown-item" data-toggle="modal" data-target="#exampleModalCenter{{$i}}" href="">Edit Metadata</a> -->
        <a href="/metadata/delete/{{$link}}" onclick="return confirm('You want to deleted this metadata.\nAre you sure?')" class="dropdown-item">Delete Metadata</a>
        <a href="/metadata/rel_meta/{{$link}}/0" class="dropdown-item">Add Metadata</a> 
        </div>
        </td>
    </tr>

    <!-- Modal -->
<div class="modal fade" id="exampleModalCenter{{$i}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content bg-light">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Edit Metadata</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      {!! Form::open(array('url' => 'metadata/edit','class'=>'form-inline','id'=>'loginform','role'=>'form', 'method'=>'post', 'enctype'=>'multipart/form-data')) !!}
        <div class="modal-body">
            <div class="container">
                <div class="row">
                <div class="col-md-12 rounded py-2 mt-2">  
                
                <div class="form-group my-3">
                <p style="padding-left: 12px;">Title:</p>
                <input type="text" class="form-control col-12" value="{{$d->title}}" name="title" placeholder="Title">
                </div>
                <div class="form-group my-3">
                <p style="padding-left: 12px;">Author:</p>
                <input type="text" class="form-control col-12" value="{{$d->author}}" name="author" placeholder="Author">
                </div> 
                <div class="form-group my-3">
                <p style="padding-left: 12px;">Modification Date:</p>
                <input type="text" class="form-control col-12" value="{{$d->moddate}}" name="moddate" placeholder="Modification Date">
                </div>
                <div class="form-group my-3">
                <p style="padding-left: 12px;">Creation Date:</p>
                <input type="text" class="form-control col-12" value="{{$d->creationdate}}" name="creationdate" placeholder="Creation Date">
                </div> 
                <div class="form-group my-3">
                <p style="padding-left: 12px;">No. of Pages:</p>
                <input type="text" class="form-control col-12" value="{{$d->pages}}" name="pages" placeholder="No. of Pages">
                </div>
                <div class="form-group my-3">
                <input type="hidden" class="form-control col-12" value="{{$d->url}}" name="url">
                </div> 
                <!-- <div class="form-group my-3"> 
                </div>
                <div class="form-group my-3">
                <button type="submit" class="btn btn-warning mb-2">Submit Metadata</button>
                </div>   -->
                 
                </div>  
                </div> 
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-warning">Save changes</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

<?php $i = $i + 1; ?>
  @endforeach
    </tbody>
</table>
@endif
</div>





@endsection
 

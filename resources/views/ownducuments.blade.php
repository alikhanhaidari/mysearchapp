@extends('layouts.app')

@section('content')
<script>
$(document).ready(function () {
$('#dtBasicExample22').DataTable();
// $('.dataTables_length').addClass('bs-select');
</script>
<div class="container table-responsive py-2 bg-light my-5" style="min-height:500px">
@if(isset($data))
<table id="dtBasicExample22" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
  <thead>
    <tr style="background-color: black; color: white;">
      <th class="th-sm">#</th>
      <th class="th-sm">File Name</th>
      <th class="th-sm">Title</th>
      <th class="th-sm">Author</th>
      <th class="th-sm">Actions</th>
    </tr>
  </thead>
  <tbody>
  <?php $i = 1; ?>
  @foreach ($data as $d)
  <?php $link = urlencode($d->url);?>
    <tr>
        <td>{{$i}}</td>
        <td><a href="/openfile/{{$link}}" target="_blank" class="dropdown-item">{{$d->filename}}</a></td>
        <td>{{$d->title}}</td>
        <td>{{$d->author}}</td>
        <td>
        <div class="dropdown show">
        <a class="btn btn-dark dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
           Select Action
        </a> 
        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
        <a href="/openfile/{{$link}}" target="_blank" class="dropdown-item">open</a>
        <a href="/openfolder/{{$link}}" target="_blank" class="dropdown-item">open folder</a> 
        <!-- <a href="/metadata/edit/{{$link}}" class="dropdown-item" data-toggle="modal" data-target="#exampleModalCenter">Edit Metadata</a>  -->
        <!-- <a class="dropdown-item" data-toggle="modal" data-target="#exampleModalCenter{{$i}}" href="">Edit Metadata</a> -->
        <a href="/metadata/delete/{{$link}}" onclick="return confirm('You want to deleted this metadata.\nAre you sure?')" class="dropdown-item">Delete Metadata</a>
        <a href="/metadata/rel_meta/{{$link}}/1" class="dropdown-item">Add Metadata</a> 
        </div>
        </td>
    </tr>

    <!-- Modal -->
<div class="modal fade" id="exampleModalCenter{{$i}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content bg-light">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Edit Metadata</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      {!! Form::open(array('url' => 'metadata/edit','class'=>'form-inline','id'=>'loginform','role'=>'form', 'method'=>'post', 'enctype'=>'multipart/form-data')) !!}
        <div class="modal-body">
            <div class="container">
                <div class="row">
                <div class="col-md-12 rounded py-2 mt-2">  
                
                <div class="form-group my-3">
                <p style="padding-left: 12px;">Title:</p>
                <input type="text" class="form-control col-12" value="{{$d->title}}" name="title" placeholder="Title">
                </div>
                <div class="form-group my-3">
                <p style="padding-left: 12px;">Author:</p>
                <input type="text" class="form-control col-12" value="{{$d->author}}" name="author" placeholder="Author">
                </div> 
                <div class="form-group my-3">
                <p style="padding-left: 12px;">Modification Date:</p>
                <input type="text" class="form-control col-12" value="{{$d->moddate}}" name="moddate" placeholder="Modification Date">
                </div>
                <div class="form-group my-3">
                <p style="padding-left: 12px;">Creation Date:</p>
                <input type="text" class="form-control col-12" value="{{$d->creationdate}}" name="creationdate" placeholder="Creation Date">
                </div> 
                <div class="form-group my-3">
                <p style="padding-left: 12px;">No. of Pages:</p>
                <input type="text" class="form-control col-12" value="{{$d->pages}}" name="pages" placeholder="No. of Pages">
                </div>
                <div class="form-group my-3">
                <input type="hidden" class="form-control col-12" value="{{$d->url}}" name="url">
                </div> 
                <!-- <div class="form-group my-3"> 
                </div>
                <div class="form-group my-3">
                <button type="submit" class="btn btn-warning mb-2">Submit Metadata</button>
                </div>   -->
                 
                </div>  
                </div> 
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-warning">Save changes</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

<?php $i = $i + 1; ?>
  @endforeach
    </tbody>
</table>
@endif
</div>





@endsection
 

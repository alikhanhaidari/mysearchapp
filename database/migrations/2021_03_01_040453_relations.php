<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Relations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('CREATE VIEW relation AS
        SELECT md.id, md.author, md.title, md.moddate, md.creationdate, md.pages, md.url, md.extension, md.filename, md.mime, md.size, md.purpose, md.keywords, md.comment, md.user_id, md.created_at, md.updated_at, rmd.cid, rmd.rid
        FROM documents AS md, relation_metadata AS rmd
        WHERE md.id = rmd.rid');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS relation');
    }
}

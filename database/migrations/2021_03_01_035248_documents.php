<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Documents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('author')->nullable()->default(null);
            $table->string('title')->nullable()->default(null);
            $table->string('moddate')->nullable()->default(null);
            $table->string('creationdate')->nullable()->default(null);
            $table->string('pages')->nullable()->default(null);
            $table->string('url')->unique()->nullable()->default(null);
            $table->string('extension')->nullable()->default(null);
            $table->string('filename')->nullable()->default(null);
            $table->string('mime')->nullable()->default(null);
            $table->string('size')->nullable()->default(null);
            $table->string('purpose')->nullable()->default(null);
            $table->string('keywords')->nullable()->default(null);
            $table->string('comment')->nullable()->default(null);
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('documents');
    }
}

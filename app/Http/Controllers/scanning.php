<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;

use App\docxmetadata;
use App\Http\Requests;
use App\Http\Controllers\Controller; 
use App\Document;
use Session;
use ZipArchive;

class scanning extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function scanning()
    {
        return view("listalldirectories");
    }

    public function openfile($path)
    {  
        $ext = pathinfo(urldecode($path));
        $dn = $ext['dirname'];
        $fn = $ext['filename'];
        $ex = $ext['extension']; 

        $cho2 = $fn.'.'.$ex.'';
        
        chdir($ext['dirname']);
        exec('start '.$cho2);

    }
    public function openfolder($path)
    { 
        $ext = pathinfo(urldecode($path));  

        chdir($ext['dirname']);
        exec("start .");
        // return redirect('scanning'); 
    }

    public function scanning_folders()
    {
        if (Input::has('path')) {
            $this->path = Input::get('path');   
            $con = array();
            $con = $this->getDirContents($this->path);
            
            $contents = array();
            foreach ($con as $entry) 
            {
                $ext = pathinfo($entry);
                if (isset($ext['extension']) && (($ext['extension'] == 'pdf')  || ($ext['extension'] == 'txt') || ($ext['extension'] == 'docx'))) { 
                    $subcontents = [
                        "entity" => $entry,
                        "basename" => $ext['basename'],
                        "size" => (string) round((filesize($entry)/1024),2),
                        "type" => $this->mimetype($entry),
                    ];
                    $contents[] = $subcontents;
                }
            } 
  
            return view("lists")->with('contents', $contents);
         }
         else
         {
            return view("lists");
         } 
    }

    public function mimetype($entry)
    {
        if(mime_content_type($entry) == "application/vnd.openxmlformats-officedocument.wordprocessingml.document") {
            return "application/docx";
        }else {
            return mime_content_type($entry);
        }
    }

    public function getDirContents($dir)
    { 
    $handle = opendir($dir);
    if ( !$handle ) return array();
    $contents = array();
    while ( $entry = readdir($handle) )
    {
        if ( $entry=='.' || $entry=='..' ) continue;

        $entry = $dir.DIRECTORY_SEPARATOR.$entry;
        if ( is_file($entry) )
        {
            $contents[] = $entry;
        }
        else if ( is_dir($entry) )
        {
        $contents = array_merge($contents, $this->getDirContents($entry));
        }
    }
    closedir($handle);
    return $contents;
    }


    public function extracting($path)
    {
        $author = "";
        $title = "";
        $moddate = "";
        $creationdate = "";
        $pages = "1";
        $keywords = "";
        // $description = "";

        $data = array();

        $url = urldecode($path);
        $ext = pathinfo($url);

        if ($ext['extension'] == "txt") {

            $author = "Administrator";
            $title = $ext['filename'];
            $moddate = date("Y/m/d H:i:s", filemtime($url));
            $creationdate = date("Y/m/d H:i:s", filectime($url)); 
            
        } else if ($ext['extension'] == "pdf") {
             
            $parser = new \Smalot\PdfParser\Parser();
            $pdf    = $parser->parseFile($url); 
            $details  = $pdf->getDetails();

            if(isset($details['Author'])) { $author = $details['Author']; }
            if(isset($details['Title']))  { $title = $details['Title']; }
            if(isset($details['Keywords']))  { if (is_array($details['Keywords'])) { $keywords = implode(', ', $details['Keywords']);}else{ $keywords = $details['Keywords'];}}
            if(isset($details['ModDate']))  { $moddate = date_format(date_create($details['ModDate']),"Y/m/d H:i:s"); }
            if(isset($details['CreationDate']))  { $creationdate = date_format(date_create($details['CreationDate']),"Y/m/d H:i:s"); }
            if(isset($details['Pages']))  { $pages = $details['Pages']; }

            // $realpages  = $pdf->getPages();

            // if ($realpages) {
            //     $description = $realpages[0]->getText();
            // }


        }else
        {

            $docxmeta = new docxmetadata();
            
            $docxmeta->setDocument($url);
    
            if($docxmeta->getCreator() != null) { $author = $docxmeta->getCreator(); }
            if($docxmeta->getTitle() != null)  { $title = $docxmeta->getTitle(); }
            if($docxmeta->getKeywords() != null)  { $keywords = $docxmeta->getKeywords(); }
            // if($docxmeta->getPages() != null)  { $pages = $docxmeta->getPages(); }
            // if($docxmeta->getDescription() != null)  { $description = $docxmeta->getDescription(); }
            if($docxmeta->getDateModified() != null)  { $moddate = date_format(date_create($docxmeta->getDateModified()),"Y/m/d H:i:s"); }
            if($docxmeta->getDateCreated() != null)  { $creationdate = date_format(date_create($docxmeta->getDateCreated()),"Y/m/d H:i:s"); }

            $zip = new ZipArchive;
            $res = $zip->open($url);
            $xml = new \DOMDocument();
            $xml->loadXML($zip->getFromName("docProps/app.xml"));

            $pages = $xml->getElementsByTagName('Pages')->item(0)->nodeValue;

        }
    
        $data = [
            'Author' => $author,
            'Title' => $title,
            'ModDate' => $moddate,
            'CreationDate' => $creationdate,
            'Pages' => $pages, 
            'Keywords' => $keywords,
            'url' => $url,
        ];

        

        

        return view("extract")->with('data', $data);

    }

    public function extracted()
    {
        $title = ''; 
        $author = '';
        $moddate = '';
        $creationdate = '';
        $pages = '';
        // $keywords = '';
        // $description = '';
        $url =  ''; 
        // $ext =  '';   
        // $extension = '';
        $filename = '';
        // $mime = '';
        // $size = '';

            if(Input::has('title')) { $title = Input::get('title'); }
            if(Input::has('author')) { $author = Input::get('author'); }
            if(Input::has('moddate')) { $moddate = Input::get('moddate'); }
            if(Input::has('creationdate')) { $creationdate = Input::get('creationdate'); }
            if(Input::has('pages')) { $pages = Input::get('pages'); }
            if(Input::has('url')) { $url = Input::get('url'); }
            // if(Input::has('keywords')) { $keywords = Input::get('keywords'); }
            // if(Input::has('description')) { $description = Input::get('description'); }
            if(Input::has('url')) { 
                $url = Input::get('url');
                $ext = pathinfo($url);   
            //     $extension = $ext['extension']; 
                $filename = $ext['filename']; 
            //     $size = round((filesize($url)/1024), 2);
            //     $mime =  $this->mimetype($url); 
            
            }



            $md = new Document;
 
            $md->title = $title;
            $md->author = $author;
            $md->moddate = $moddate;
            $md->creationdate = $creationdate;
            $md->pages = $pages;
            // $md->keywords = $keywords;
            // $md->description = $description;
            $md->url = $url;
            // $md->extension = $extension;
            $md->filename = $filename;
            // $md->mime = $mime;
            // $md->size = $size;
            $md->user_id = Auth::User()->id;

            try{ 
                $md->save(); 

                Session::flash('message', "saved successfully");
                return \Redirect::back();
             }
             catch(\Exception $e){ 
                
                Session::flash('error', "this file exist already");
                return \Redirect::back();
                
             }

            }
}
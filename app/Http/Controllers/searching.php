<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;

use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller; 
use App\Document;

class searching extends Controller
{ 

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function search()
    {
        return view("searching");
    }

    public function searching()
    {
        $filename = null;
        $title = null;
        $author = null;
        $pages = null;
        $extension = null;
        $size = null;
        $keywords = 'NULL';

        if (Input::has('filename')) { $filename = Input::get('filename'); }
        if (Input::has('title')) { $title = Input::get('title'); }
        if (Input::has('author')) { $author = Input::get('author'); }
        if (Input::has('pages')) { $pages = Input::get('pages'); }
        if (Input::has('extension')) { $extension = Input::get('extension'); }
        if (Input::has('size')) { $size = Input::get('size'); }
        if (Input::has('keywords')) { $keywords = Input::get('keywords'); }

        $uid = Auth::User()->id;

        $data = DB::select("SELECT * FROM `documents` WHERE `filename` = ? AND `user_id` = ?
        UNION 
        SELECT * FROM `documents` WHERE `author` = ?  AND `user_id` = ?
        UNION
        SELECT * FROM `documents` WHERE `title` = ?  AND `user_id` = ?
        UNION 
        SELECT * FROM `documents` WHERE `pages` = ?  AND `user_id` = ?
        UNION
        SELECT * FROM `documents` WHERE `extension` = ?  AND `user_id` = ?
        UNION
        SELECT * FROM `documents` WHERE `size` = ?  AND `user_id` = ?
        UNION 
        SELECT * FROM `documents` WHERE `keywords` LIKE ? AND `user_id` = ?", [$filename, $uid, $author, $uid, $title, $uid, $pages, $uid, $extension, $uid, $size, $uid, '%'.$keywords.'%', $uid ]);

        foreach($data as $d)
        { 
            $data1 = DB::table('relation')->where('user_id', Auth::User()->id)->where('cid', $d->id)->get(); 

            $data = array_merge($data, $data1);
        }
        return view("searching")->with('data', $data);
    }

    public function filtersearch(Request $request)
    {
        $value = $request->value;
        $column = $request->column;

        $value = '%'.$value.'%';
        $data = DB::table('documents')->where($column, 'LIKE' , $value)->groupBy($column)->get();

        return response()->json(['data'=>$data,'column'=>$column, 'value'=>$value]);
    }


    public function owndocuments()
    {
        $data = DB::table('documents')->where('user_id', Auth::User()->id)->get();

        return view("ownducuments")->with('data', $data);
    }


    public function allmetadata($path)
    { 
        $url = urldecode($path);
        $cobj = DB::table('documents')->where('url', $url)->get();
        

        return view("allmetadata")->with('cobj',$cobj);

    }
}

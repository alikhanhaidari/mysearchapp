<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

use App\docxmetadata;
use App\Http\Requests;
use App\Http\Controllers\Controller; 
use App\Document;
use Session;

class metadata extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function edit()
    {
        // echo 'edit ==>  '.urldecode($path);
        if(Input::has('url')){
            $url = Input::get('url');
            $title = Input::get('title');
            $author = Input::get('author');
            $moddate = Input::get('moddate');
            $creationdate = Input::get('creationdate');
            $pages = Input::get('pages'); 

            $md = DB::table('documents')->where('url', $url)->get();

            // print($md[0]->id);
            

            try{ 
                // $md[0]->save(); 
                DB::update('UPDATE `documents` SET `author`=?,`title`=?,`moddate`=?,`creationdate`=?,`pages`=? WHERE `id` = ?', [$author, $title, $moddate, $creationdate, $pages, $md[0]->id]);
                // echo 'edit done';
                return redirect()->back();
             }
             catch(\Exception $e){ 
                echo $e->getCode();
                // echo "this file can't be edit";
                return redirect()->back();
             }

            
        }else{
            // echo 'no file to edit';
            return redirect()->back();
        }

    }

    public function delete($path)
    {
        $url = urldecode($path);
        $data = DB::table('documents')->where('url', $url)->delete();

        return redirect()->back();
    }

    public function rel_meta($path, $option)
    { 
        $url = urldecode($path);

        $this->extracting($path, $option);

        $cobj = DB::table('documents')->where('url', $url)->get();

        $relobj = DB::select('SELECT * FROM `documents` WHERE `id` IN (SELECT `rid` FROM `relation_metadata` WHERE `cid` = ?)', [$cobj[0]->id]);
        
        $data = DB::table('documents')->where('user_id', Auth::User()->id)->get();
        

        return view("relative")->with('data', $data)->with('cobj',$cobj)->with('relobj',$relobj)->with('option',$option);

    }

    public function addRelation($path, $cid, $rid)
    { 
        DB::insert('INSERT INTO `relation_metadata`(`id`, `cid`, `rid`) VALUES (?,?,?)', [0, $cid, $rid]);
        return redirect()->back();
    }

    public function removeRelation($path, $cid, $rid)
    { 
        DB::delete('DELETE FROM `relation_metadata` WHERE `cid` = ? AND `rid` = ?', [$cid, $rid]);
        return redirect()->back();
    }

    

    public function purpose()
    { 
        if(Input::has('url')){
            $purpose = Input::get('purpose');
            $keywords = Input::get('keywords'); 
            $url = Input::get('url');

            $md = DB::table('documents')->where('url', $url)->get();

            try{ 
                DB::update('UPDATE `documents` SET `purpose`=?, `keywords`=? WHERE `id` = ?', [$purpose,$keywords, $md[0]->id]);
                
                Session::flash('message', "saved successfully");
                return redirect()->back();
             }
             catch(\Exception $e){ 
                
                return redirect()->back();
             }

            
        }else{
            
            return redirect()->back();
        }
    }
    public function comment()
    { 
        if(Input::has('url')){
            $comment = Input::get('comment');
            $url = Input::get('url');

            $md = DB::table('documents')->where('url', $url)->get();

            try{ 
                DB::update('UPDATE `documents` SET `comment`=? WHERE `id` = ?', [$comment, $md[0]->id]);

                Session::flash('message', "saved successfully");
                
                return redirect()->back();
             }
             catch(\Exception $e){ 
                
                return redirect()->back();
             }

            
        }else{
            
            return redirect()->back();
        }
    }



    public function extracting($path, $option)
    {
        
        $keywords = null;
        $ext =  '';   
        $extension = '';
        $filename = '';
        $mime = '';
        $size = '';

        $url = urldecode($path);
        $ext = pathinfo($url);

        if ($option == 0) {

        if ($ext['extension'] == "txt") {
            
        } else if ($ext['extension'] == "pdf") {
             
            $parser = new \Smalot\PdfParser\Parser();
            $pdf    = $parser->parseFile($url); 
            $details  = $pdf->getDetails();

            if(isset($details['Keywords']))  { if (is_array($details['Keywords'])) { $keywords = implode(', ', $details['Keywords']);}else{ $keywords = $details['Keywords'];}}

        }else
        {
            $docxmeta = new docxmetadata();
            
            $docxmeta->setDocument($url);
            if($docxmeta->getKeywords() != null)  { $keywords = $docxmeta->getKeywords(); }
           
        }
  
        $extension = $ext['extension']; 
        $filename = $ext['filename']; 
        $size = round((filesize($url)/1024), 2);
        $mime =  $this->mimetype($url); 


        
            DB::update('UPDATE `documents` SET `extension`=?,`filename`=?,`mime`=?,`size`=?,`keywords`=? WHERE `url` = ?', [$extension, $filename, $mime, $size,$keywords, $url]);
        } 
        // else {
        //     DB::update('UPDATE `documents` SET `extension`=?,`filename`=?,`mime`=?,`size`=? WHERE `url` = ?', [$extension, $filename, $mime, $size, $url]);
        
        // }

    }

    public function mimetype($entry)
    {
        if(mime_content_type($entry) == "application/vnd.openxmlformats-officedocument.wordprocessingml.document") {
            return "application/docx";
        }else {
            return mime_content_type($entry);
        }
    }

}

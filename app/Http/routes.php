<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

// use App\Task;
// use Illuminate\Http\Request;  

    // Route::get('/', 'index@home');
    Route::get('/', 'searching@search');
    Route::post('/', 'searching@searching');
    Route::post('/filtersearch', 'searching@filtersearch');
    Route::get('/owndocuments', 'searching@owndocuments');
    Route::get('/allmetadata/{path}', 'searching@allmetadata');

    Route::get('/scanning', 'scanning@scanning');
    Route::post('/scanning1', 'scanning@scanning_folders');
    Route::get('/openfile/{path}', 'scanning@openfile');
    Route::get('/openfolder/{path}', 'scanning@openfolder');
    Route::get('/metadata/extract/{path}', 'scanning@extracting');
    Route::post('/metadata/extracted', 'scanning@extracted');

    Route::post('/metadata/edit', 'metadata@edit');
    Route::get('/metadata/delete/{path}', 'metadata@delete');
    Route::get('/metadata/rel_meta/{path}/{option}', 'metadata@rel_meta');

    Route::post('/metadata/edit/2', 'metadata@purpose');
    // Route::post('/metadata/edit/3', 'metadata@location');
    // Route::post('/metadata/edit/4', 'metadata@rights');
    Route::post('/metadata/edit/5', 'metadata@comment');

    Route::get('/addRelation/{path}/{cid}/{rid}', 'metadata@addRelation');
    Route::get('/removeRelation/{path}/{cid}/{rid}', 'metadata@removeRelation');

// Route::group(['middleware' => ['web']], function () {

    

//     /**
//      * Show Task Dashboard
//      */
//     Route::get('/', function () {
//         return view('tasks', [
//             'tasks' => Task::orderBy('created_at', 'asc')->get()
//         ]);
//     });

//     /**
//      * Add New Task
//      */
//     Route::post('/task', function (Request $request) {
//         $validator = Validator::make($request->all(), [
//             'name' => 'required|max:255',
//         ]);

//         if ($validator->fails()) {
//             return redirect('/')
//                 ->withInput()
//                 ->withErrors($validator);
//         }

//         $task = new Task;
//         $task->name = $request->name;
//         $task->save();

//         return redirect('/');
//     });

//     /**
//      * Delete Task
//      */
//     Route::delete('/task/{id}', function ($id) {
//         Task::findOrFail($id)->delete();

//         return redirect('/');
//     });
// });

Route::auth();

Route::get('/home', 'HomeController@index');

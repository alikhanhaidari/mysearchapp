<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $fillable = [
        'author', 'title', 'moddate','creationdate', 'pages', 'url', 'extension', 'filename', 'mime',
         'size', 'purpose', 'keywords','comment'
    ];

    protected $table = 'documents';


    public function user()
    {
        $this->belongsTo(User::class);
    }
}

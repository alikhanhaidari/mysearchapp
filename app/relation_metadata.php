<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class relation_metadata extends Model
{
    protected $fillable = [
        'cid', 'rid',
    ];

    protected $table = 'relation_metadata';
}

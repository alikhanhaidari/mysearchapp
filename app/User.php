<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Metadata;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function documents()
    {
        $this->hasMany(Metadata::class);
    }
}

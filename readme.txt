how to run

pre-requisit:
1: php 
2: composer

procedure:
1: extract the archieve
2: open cmd
3: cd to the ../quickstart directory (home directory of your project)
4: run composer install
5: run composer update
6: run 'php artisan serve'
7: show see some line like http://localhost:8000
8: open http://localhost:8000 in browser
